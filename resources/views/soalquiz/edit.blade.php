@extends('layout.master')

@section('judul')
Create Data Game
@endsection

@section('judul2')
Data Game
@endsection

@section('content')

<form action="/game/{{$game->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Nama</label>
        <input type="text" class="form-control" value="{{$game->nama}}" name="nama" placeholder="Masukkan Nama">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Gameplay</label>
        <input type="text" class="form-control" value="{{$game->gameplay}}" name="gameplay" placeholder="Masukkan Usia">
        @error('gameplay')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Developer</label>
        <input type="text" class="form-control" value="{{$game->developer}}" name="developer" placeholder="Masukkan Dev">
        @error('developer')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Year</label>
        <input type="number" class="form-control" value="{{$game->year}}" name="year" placeholder="Masukkan Year">
        @error('year')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
</form>
@endsection