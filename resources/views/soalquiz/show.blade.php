@extends('layout.master')

@section('judul')
Detail data Game : {{$game->nama}}
@endsection

@section('judul2')
Details
@endsection

@section('content')
<h3>Gameplay : {{$game->gameplay}}</h3>
<h3>Developer : {{$game->developer}}</h3>
<h3>Year : {{$game->year}}</h3>
@endsection