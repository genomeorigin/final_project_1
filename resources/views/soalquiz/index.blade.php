@extends('layout.master')

@section('judul')
All Game
@endsection

@section('judul2')
List Game
@endsection

@section('content')

<a href="/game/create" class="btn btn-primary mb-2">Tambah</a>

{{-- isi link a href menuju ke form tambah data --}}

<table class="table">

<thead class="thead-light">

<tr>

<th scope="col">#</th>

<th scope="col">Name</th>

<th scope="col">Gameplay</th>

<th scope="col">Developer</th>

<th scope="col">Year</th>

<th scope="col" >Actions</th>

</tr>

</thead>

<tbody>
    @forelse ($game as $key=>$value)
    <tr>
        <td>{{$key + 1}}</th>
        <td>{{$value->nama}}</td>
        <td>{{$value->gameplay}}</td>
        <td>{{$value->developer}}</td>
        <td>{{$value->year}}</td>
        <td>
            
            <form action="/game/{{$value->id}}" method="POST">
                <a href="/game/{{$value->id}}" class="btn btn-info">Details</a>
                <a href="/game/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                @csrf
                @method('DELETE')
                <input type="submit" class="btn btn-danger my-1" value="Delete">
            </form>
        </td>
    </tr>
@empty
    <tr colspan="3">
        <td>No data</td>
    </tr>  
@endforelse

{{-- code disini tampilakan semua data di database dan buat link dan tombol untuk edit, detail, dan delete --}}

</tbody>

</table>
@endsection