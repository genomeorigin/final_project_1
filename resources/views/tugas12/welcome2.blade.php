@extends('layout.master')

@section('judul')
PKS Digital School
@endsection

@section('judul2')
Selamat Datang
@endsection

@section('content')
  <h1> SELAMAT DATANG! {{$fname}} {{$lname}}</h1>
  <h3> Terima kasih telah bergabung di Website Kami. Media Belajar kita bersama! </h3>
  <h3> {{$biodata}} </h3>
@endsection
