@extends('layout.master')

@section('judul')
Detail Film id ke {{$film->id}}
@endsection

@section('judul2')
Film Box Catering
@endsection

@section('content')

<img src="{{asset('images/'.$film->poster)}}" height="650px" alt="">
<h4>{{$film->judul}}</h4>
<p>{{$film->ringkasan}}</p>

<h3>Review : </h3>

@forelse ($film->kritik as $item)
<div class="card">
    <div class="card-body">
      <small><b>{{$item->user->name}}</b></small>
      <p class="card-text">{{$item->isi}}.</p>
    </div>
  </div>
    
@empty
    <h4>Tidak Ada Review</h4>
@endforelse

<form action="/kritik" method="POST">
    @csrf
    
    <div class="form-group">
        <label>Isi Review</label>
        <input type="hidden" value="{{$film->id}}" name="film_id">
        <textarea name="isi" class="form-control" cols="30" rows="10"></textarea>
        @error('isi')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Poin</label>
        <input type="number" class="form-control" name="poin">
        @error('poin')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary my-3">Submit</button>
</form>

<a href="/film" class="btn btn-secondary">Kembali</a>

@endsection