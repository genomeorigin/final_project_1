@extends('layout.master')

@section('judul')
List Film
@endsection

@section('judul2')
Film Box Catering
@endsection

@section('content')
@auth
<a href="/film/create" class="btn btn-primary my-2">Tambah Film</a>
@endauth

<div class='row'>
    @forelse ($film as $item)
    <div class="col-3">
        <div class="card">
            <img src="{{asset('images/'.$item->poster)}}" height="400px" class="card-img-top" alt="..." >
            <div class="card-body">
            <span class="badge badge-info">{{$item->genre->nama}}</span>
              <h3>{{Str::limit($item->judul, 15)}}</h3>
              <p class="card-text">{{Str::limit($item->ringkasan, 100)}}</p>
              @auth
              <form action="/film/{{$item->id}}" method="POST">
                @csrf
                @method('Delete')
                    <a href="/film/{{$item->id}}" class="btn btn-info btn-sm">Details</a>
                    <a href="/film/{{$item->id}}/edit" class="btn btn-success btn-sm">Edit</a>
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
              </form>
              @endauth

              @guest
              <a href="/film/{{$item->id}}" class="btn btn-info btn-sm">Details</a>
              @endguest
            </div>
        </div>
    </div>
    @empty
        <h1>Data Film Masih Kosong</h1>
    @endforelse
</div>

@endsection