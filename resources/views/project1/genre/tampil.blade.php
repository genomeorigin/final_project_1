@extends('layout.master')

@section('judul')
Genre {{$genre->nama}}
@endsection

@section('judul2')
Details
@endsection

@section('content')

<h1>{{$genre->nama}}</h1>
{{-- <p>Usia : {{$cast->umur}}</p>
<p>Biodata : {{$cast->bio}}</p> --}}

<h3>List Film</h3>

<div class="row">
    @foreach ($genre->film as $item)
    <div class="col-3">
        <div class="card">
            <img src="{{asset('images/'.$item->poster)}}" height="400px" class="card-img-top" alt="..." >
            <div class="card-body">
              <h3>{{Str::limit($item->judul, 18)}}</h3>
              <p class="card-text">{{Str::limit($item->ringkasan, 100)}}</p>
              <a href="/film/{{$item->id}}" class="btn btn-info btn-sm">Details</a>
              
            </div>
        </div>
    </div>
    @endforeach
</div>


@endsection