@extends('layout.master')

@section('judul')
Genre Film
@endsection

@section('judul2')
Isi Disini
@endsection

@section('content')
<form action="/genre" method="POST">
    @csrf
    <div class="form-group">
        <label>Genre Film</label>
        <input type="text" class="form-control" name="nama">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    {{-- </div>
    <div class="form-group">
        <label>Umur</label>
        <input type="text" class="form-control" name="umur" placeholder="Masukkan Usia">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Biodata</label>
        <input type="text" class="form-control" name="bio" placeholder="Masukkan Bio">
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div> --}}
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>
@endsection