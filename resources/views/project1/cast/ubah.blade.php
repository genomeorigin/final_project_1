@extends('layout.master')

@section('judul')
Edit Data Pemain {{$cast->nama}}
@endsection

@section('judul2')
Edit Data
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Nama</label>
        <input type="text" class="form-control" value="{{$cast->nama}}" name="nama" placeholder="Masukkan Nama">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Umur</label>
        <input type="text" class="form-control" value="{{$cast->umur}}" name="umur" placeholder="Masukkan Usia">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Biodata</label>
        <input type="text" class="form-control" value="{{$cast->bio}}" name="bio" placeholder="Masukkan Bio">
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
</form>
@endsection