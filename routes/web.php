<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Tugas 12
Route::get('/index', 'IndexController@index');
Route::get('/registerr', 'AuthController@form');
Route::post('/welcome2', 'AuthController@kirim');

// Tugas 13
Route::get('/table', function () {
    return view('/tugas13/table');
});

Route::get('/data-tables', function () {
    return view('/tugas13/datatables');
});

// Route::get('/master', function(){
//     return view('layout.master');
// });

// Soal Quiz 2 CRUD Game
Route::get('/game/create', 'GameController@buat');
Route::post('/game', 'GameController@tambah');
Route::get('/game', 'GameController@index');
Route::get('/game/{game_id}', 'GameController@tampil');
Route::get('/game/{game_id}/edit', 'GameController@ubah');
Route::put('/game/{game_id}', 'GameController@perbarui');
Route::delete('/game/{game_id}', 'GameController@hapus');

// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', function () {
    return view('index');
});

Route::middleware('auth')->group(function () {
// CRUD Genre
Route::get('/genre/create', 'GenreController@buat');
Route::post('/genre', 'GenreController@tambah');
Route::get('/genre', 'GenreController@indexx');
Route::get('/genre/{genre_id}', 'GenreController@tampil');
Route::get('/genre/{genre_id}/edit', 'GenreController@ubah');
Route::put('/genre/{genre_id}', 'GenreController@perbarui');
Route::delete('/genre/{genre_id}', 'GenreController@hapus');

// CRUD Cast
Route::get('/cast/create', 'CastController@buat');
Route::post('/cast', 'CastController@tambah');
Route::get('/cast', 'CastController@indexx');
Route::get('/cast/{cast_id}', 'CastController@tampil');
Route::get('/cast/{cast_id}/edit', 'CastController@ubah');
Route::put('/cast/{cast_id}', 'CastController@perbarui');
Route::delete('/cast/{cast_id}', 'CastController@hapus');

// Update Profile
Route::resource('profile', 'ProfileController')->only([
    'index', 'update'
]);

// Review
Route::resource('kritik', 'ReviewController')->only([
    'store'
]);

});

// CRUD Film
Route::resource('film', 'FilmController');

Auth::routes();