<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Genre;

class GenreController extends Controller
{
    public function buat(){
        return view('/project1/genre/create');
    }

    public function tambah(Request $request){
        $request->validate([
            'nama' => 'required',      
            // 'umur' => 'required',
            // 'bio' => 'required',
        ]);

        $query = DB::table('genre')->insert(
        [
            "nama" => $request["nama"],
            // "umur" => $request["umur"],
            // "bio" => $request["bio"],
        ]);

        return redirect('/genre');
    }

    public function indexx(){
        $genre = Genre::all();

        return view('/project1/genre/index', compact('genre'));
    }

    public function tampil($id){
        $genre = Genre::find($id);

        return view('/project1/genre/tampil', compact('genre'));
    }

    public function ubah($id){
        $genre = DB::table('genre')->where('id', $id)->first();

        return view('/project1/genre/ubah', compact('genre'));
    }

    public function perbarui($id, Request $request){
        $request->validate([
            'nama' => 'required',      
            // 'umur' => 'required',
            // 'bio' => 'required',
        ]);

        $affected = DB::table('genre')
              ->where('id', $id)
              ->update(
                [
                    'nama' => $request['nama'],
                    // 'umur' => $request['umur'],
                    // 'bio' => $request['bio']
                ]
            );
        
        return redirect('/genre');
    }

    public function hapus($id){
        DB::table('genre')->where('id', $id)->delete();

        return redirect('/genre');
    }
}
