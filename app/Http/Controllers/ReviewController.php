<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Review;
use Illuminate\Support\Facades\Auth;

class ReviewController extends Controller
{
    public function store(Request $request){
        $request->validate([
            'isi' => 'required',
            'poin' => 'required'
        ]);

        $kritik = new Review;

        $kritik->isi = $request->isi;
        $kritik->poin = $request->poin;
        $kritik->user_id = Auth::id();
        $kritik->film_id = $request->film_id;

        $kritik->save();

        return redirect()->back();
    }
}
