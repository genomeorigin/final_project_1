<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class GameController extends Controller
{
    public function buat(){
        return view('/soalquiz/create');
}

public function tambah(Request $request){
    $request->validate([
        'nama' => 'required',      
        'gameplay' => 'required',
        'developer' => 'required',
        'year' => 'required',
    ]);

    $query = DB::table('game')->insert(
    [
        "nama" => $request["nama"],
        "gameplay" => $request["gameplay"],
        "developer" => $request["developer"],
        "year" => $request["year"],
    ]);

    return redirect('/game');
}

public function index(){
    $game = DB::table('game')->get();

    return view('/soalquiz/index', compact('game'));
}

public function tampil($id){
    $game = DB::table('game')->where('id', $id)->first();

    return view('/soalquiz/show', compact('game'));
}

public function ubah($id){
    $game = DB::table('game')->where('id', $id)->first();

    return view('/soalquiz/edit', compact('game'));
}

public function perbarui($id, Request $request){
    $request->validate([
        'nama' => 'required',      
        'gameplay' => 'required',
        'developer' => 'required',
        'year' => 'required',
    ]);

    $affected = DB::table('game')
          ->where('id', $id)
          ->update(
            [
                'nama' => $request['nama'],
                'gameplay' => $request['gameplay'],
                'developer' => $request['developer'],
                'year' => $request['year']
            ]
        );
    
    return redirect('/game');
}

public function hapus($id){
    DB::table('game')->where('id', $id)->delete();

    return redirect('/game');
}

}